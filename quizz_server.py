import json
import sqlite3
from flask import Flask, url_for, redirect, request, Response
from gevent import pywsgi
from datetime import datetime


app = Flask(__name__)

# 输出日志
def log(content):
    print("[" + datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3] + "] " + content)


"""
用户注册
参数：
{
    "username": "",
    "userpass": ""
}
返回：
{
    "code": "",
    "msg": "",
    "data": {

    }
}
code: 
    0: 成功
    10001: 用户名已存在
    10000: 其它错误
"""
@app.route("/register", methods=['POST'])
def register():
    rsp = Response()
    rsp.headers.set("Content-Type", "application/json")
    try:
        userinfo = request.get_json()
        cursor = dbconn.cursor()
        cursor.execute("select id from user_info where username=?", (userinfo['username'],))
        data = cursor.fetchall()
        if len(data)>0:
            rsp.data = json.dumps({
                "code": "10001",
                "msg": "用户%s已经存在" % userinfo['username']
            }, ensure_ascii=False)
            return rsp
        cursor.execute("insert into user_info (username, userpass) values (?, ?)", (userinfo['username'], userinfo['userpass']))
        dbconn.commit()
        cursor.close()
        rsp.data = json.dumps({
            "code": "0",
            "msg": "注册成功"
        }, ensure_ascii=False)
        return rsp
    except Exception as ex:
        log(str(ex))
        rsp.data = json.dumps({
            "code":"1000",
            "msg": str(ex)
        }, ensure_ascii=False)
        return rsp

"""
更新分数
参数：
{
    "username": "",
    "userpass": "",
    "score": ""
}
返回：
{
    "code": "",
    "msg": "",
    "data": {

    }
}
code: 
    0: 成功
    10002: 用户验证失败
    10000: 其它错误
"""
@app.route("/score", methods=['POST'])
def updateScore():
    rsp = Response()
    rsp.headers.set("Content-Type", "application/json")
    try:
        userinfo = request.get_json()
        cursor = dbconn.cursor()
        cursor.execute("select id from user_info where username=? and userpass=?", (userinfo['username'], userinfo['userpass']))
        data = cursor.fetchall()
        if len(data)==0:
            rsp.data = json.dumps({
                "code": "10002",
                "msg": "身份认证失败"
            }, ensure_ascii=False)
            return rsp
        cursor.execute("update user_info set score=? where id=?", (int(userinfo['score']), data[0][0]))
        dbconn.commit()
        cursor.close()
        rsp.data = json.dumps({
            "code": "0",
            "msg": "分数更新成功"
        }, ensure_ascii=False)
        return rsp
    except Exception as ex:
        log(str(ex))
        rsp.data = json.dumps({
            "code":"1000",
            "msg": str(ex)
        }, ensure_ascii=False)
        return rsp

"""
获取排名
参数：
    无
返回：
{
    "code": "",
    "msg": "",
    "data": [
        {"username": "", "score": ""},
        {"username": "", "score": ""},
        {"username": "", "score": ""},
    ]
}
code: 
    0: 成功
    10000: 其它错误
"""
@app.route("/rank", methods=['GET'])
def getRank():
    rsp = Response()
    rsp.headers.set("Content-Type", "application/json")
    try:
        cursor = dbconn.cursor()
        cursor.execute("select username, score from user_info order by score desc limit 0, 20")
        data = cursor.fetchall()
        cursor.close()
        rd = []
        for r in data:
            rd.append({
                "username": r[0],
                "score": r[1]
            })
        rsp.data = json.dumps({
            "code": "0",
            "msg": "",
            "data": rd
        }, ensure_ascii=False)
        return rsp
    except Exception as ex:
        log(str(ex))
        rsp.data = json.dumps({
            "code":"1000",
            "msg": str(ex)
        }, ensure_ascii=False)
        return rsp

if __name__ == "__main__":
    # 创建数据库链接
    dbconn = sqlite3.connect("quizzdata.db", check_same_thread=False)

    # 启动web服务器
    # app.run(host = "0.0.0.0", port=15000, debug = True)
    server = pywsgi.WSGIServer(('0.0.0.0', 15000), app)
    server.serve_forever()
